<?php
//inclusion du fichier de la connection

include('src/config/connection.php');

include('src/includes/header.php');
include('src/utils/function.php');


//soumition du formaulaire
if (isset($_SESSION['email'])) {
    header('Location: /src/post/index.php');
}
if (isset($_POST['submit'])) {
    //test sur le donnée à soumèttre
    if (isset($_POST['email']) && $_POST['email']) {
        if (isset($_POST['mdp']) && $_POST['mdp']) {
            if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {

                //affectation de variable
                $email = test_input($_POST['email']);
                $mdp = test_input($_POST['mdp']);


                //verification des information de l'utilisateur dans la base de donneés

                $verif = $pdo->prepare("SELECT * FROM users WHERE email = ? ");
                $verif->execute(array($email));
                $verif_ok = $verif->rowCount();
                if ($verif_ok === 1) {
                    $user = $verif->fetch();
                    if ( password_verify($mdp, $user['password'] )){
                        $email = $user['email'];
                        $mdp = $user['mdp'];
                        $id = $user['id'];

                        $_SESSION['email'] = $email;
                        $_SESSION['id'] = $id;
                        header('Location: /src/post/index.php');
                    }else {
                        $error = "Votre adresse e-mail ou votre mot de passe est incorrect";
                    }

                } else {
                    $error = "Erreur";
                }
            } else {
                $error = "Veuillez taper une adresse e-mail valide";
            }
        } else {
            $error = "Veuillez saisir votre mot de passe";
        }
    } else {
        $error = "Veuillez saisir votre adresse email";
    }
}
?>

        <div class="md:w-1/2 w-full">
            <?php
            //affichage en cas d'erreur
            if (isset($error)) {
                echo "<div class=\" border-red-500 bg-red-300 p-2 rounded\">$error</div>";
            } else {
                echo "<br>";
            }
            ?>
            <form action="" method="post" role="form" class="">
                <h2 class="text-2xl mb-6 custom-label">Identification</h2>
                    <input name="email" class="custom-input" type="email" placeholder="email" />
                    <input name="mdp" class="custom-input" type="password" placeholder="mot de passe" />

                <div class="show_info text-sm mb-4 w-max text-red-400"></div>

                <div class="text-center">
                    <button type="submit" name="submit" class="custom-blue-button" >Connexion</button>
                </div>
                <a href="src/user/create.php" class="text-blue-500">Créer un compte</a>
            </form>
        </div>


<?php include('src/includes/footer.php'); ?>