<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>tp web</title>
    <link href="/public/output.css" rel="stylesheet">
</head>
<?php
// demarage du session
session_start();

if (isset($_POST['logout'])) {
    $_SESSION['email'] = null;
    $_SESSION['id'] = null;

    header('Location: /');
}



?>

<script>
    function toggleNav(){
        document.querySelector("#mobile-menu").classList.toggle('toggle-nav')
    }

</script>

<style>
    .toggle-nav{
        display: block;
    }
</style>

<body class="dark:bg-gray-800 dark:text-gray-200">

<nav class="bg-white border-gray-200 px-2 sm:px-4 py-2.5 rounded dark:bg-gray-800">
    <div class="container flex flex-wrap justify-between items-center mx-auto">
        <a href="/" class="flex items-center">
            <span class="self-center text-xl font-semibold whitespace-nowrap dark:text-white">TP Web</span>
        </a>
        <button onclick="toggleNav()"  type="button" class="inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="mobile-menu-2" aria-expanded="false">
            <span class="sr-only">Open main menu</span>
            <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path></svg>
            <svg class="hidden w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
        </button>
        <div class="hidden w-full md:block md:w-auto" id="mobile-menu" >
            <ul class="flex flex-col mt-4 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium">
                <?php
                if (isset($_SESSION['email'])) {
                    ?>
                    <a class="nav-link mt-3" href='../post/index.php'>Post</a>
                    <a class="nav-link mt-3" href='../xmlFile/create.php'>Genérer Xml</a>
                    <div class="flex flex-wrap">
                        <form class=" " action="" method="post">
                            <button class="m-1 border border-gray-500 p-2 rounded-xl  " type="submit" name="logout">Deconnexion</button>
                        </form>

                        <a class="m-1 border border-gray-500 p-2 rounded-xl" href='/src/user/edit.php?userId=<?= $_SESSION["id"] ?>'>Mon profil</a>

                    </div>


                    <?php

                }else{
                    ?>
                    <a class="nav-link" href='/'>Connexion</a>
                    <a class="nav-link" href='/src/user/create.php'>Créer un compte</a>

                    <?php
                }
                ?>

            </ul>
        </div>
    </div>
</nav>


<div class="container p-10 m-auto flex justify-center">

