<?php

class User
{
    private $id;
    private $email;
    private $username;
    private $password;
    private $nom;
    private $prenom;
    private $gender;
    private $add;
    public $age;
    public $dateNaiss;
    private $department = 'Computer';


    public function __construct(array $args)
    {
        foreach ($args as $key=>$data){
            $this->{$key} = $data;
        }
    }

    public function setName($nom)
    {
        $this->nom = $nom;
    }

    public function getName()
    {
        return $this->nom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    public
    function setDepartment($dep)
    {
        $this->department = $dep;
    }

    public
    function getDepartment(){
        return $this->department;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public
    function __toString()
    {
        return 'le nom est : ' . $this->nom;
    }

    public function save()
    {
        $state = true;
        $mess = "L'utilisateur a été enregistrer";
        try {
            include('../config/connection.php');

            $register = $pdo->prepare('INSERT INTO users (email, password, username) VALUE (?,?,?)');

            $register->execute(array($this->email, password_hash($this->password, PASSWORD_BCRYPT ), $this->username));

        } catch (PDOException $e){
            $state = false;
            if ($e->getCode() == 23000)
                $mess = "L'email ou le nom d'utilisateur est déjà utilisé !";
            else
                $mess = $e->getMessage();
        }
        return [$state, $mess] ;
    }

    public function update()
    {
        $state = true;
        $mess = "Les informarions on été modifiées";
        try {

            include('../config/connection.php');


            $sql = "UPDATE users SET email = '$this->email', username = '$this->username', nom = '$this->nom'
               , prenom = '$this->prenom', adresse = '$this->add', dateNaiss = ' $this->dateNaiss'  WHERE id = '$this->id'";

            // On prépare la requête
            $query = $pdo->prepare($sql);

            // On exécute
            $query->execute();

        } catch (PDOException $e){
            $state = false;
            if ($e->getCode() == 23000)
                $mess = "L'email ou le nom d'utilisateur est déjà utilisé !!!";
            else
                $mess = $e->getMessage();
        }
        return [$state, $mess] ;
    }
}