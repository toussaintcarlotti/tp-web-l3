<?php
//inclusion du fichier de la connection

include('../config/connection.php');
include('../includes/header.php');




if (isset($_SESSION['email'])) {

    // On détermine sur quelle page on se trouve
    if (isset($_GET['page']) && !empty($_GET['page'])) {
        $currentPage = (int)strip_tags($_GET['page']);
    } else {
        $currentPage = 1;
    }
    // On se connecte à là base de données

    // On détermine le nombre total d'articles
    $sql = 'SELECT COUNT(*) AS nb_articles FROM `posts`;';

    // On prépare la requête
    $query = $pdo->prepare($sql);

    // On exécute
    $query->execute();

    // On récupère le nombre d'articles
    $result = $query->fetch();

    $nbArticles = (int)$result['nb_articles'];

    // On détermine le nombre d'articles par page
    $parPage = 10;

    // On calcule le nombre de pages total
    $pages = ceil($nbArticles / $parPage);

    // Calcul du 1er article de la page
    $premier = ($currentPage * $parPage) - $parPage;

    $sql = 'SELECT * FROM `posts` LIMIT :premier, :parpage;';

    // On prépare la requête
    $query = $pdo->prepare($sql);

    $query->bindValue(':premier', $premier, PDO::PARAM_INT);
    $query->bindValue(':parpage', $parPage, PDO::PARAM_INT);

    // On exécute
    $query->execute();

    // On récupère les valeurs dans un tableau associatif
    $articles = $query->fetchAll(PDO::FETCH_ASSOC);
}else{
    header('location: /');
}

?>


<div class="grid w-full md:w-2/3">
    <?php if (isset($_SESSION['email'])) { ?>
        <div class="mb-2 custom-label">
            <h1>Bonjour <em class="dark:text-blue-400 text-blue-700"><?= $_SESSION['email']; ?></em> Vous etes connecté.</h1>
        </div>
        <div>
            <a class="bg-blue-600 rounded text-amber-50 p-2" href='create.php'>Nouveau post</a>
        </div>
        <?php

    foreach($articles as $row){

        ?>
        <div class='p-2 rounded border mt-5 inline-flex custom-input'>
            <div>
                <div class='font-bold'><?= $row['title'] ?></div>
                <div><?= $row['description'] ?></div>
            </div>
            <div class="ml-auto inline-flex">
                <?php
                if ($row['user_id'] == $_SESSION['id']){
                ?>
                <a class="rounded w-20 bg-amber-500 h-10 p-2" href="edit.php?postId=<?php echo $row['id'] ?>">Modifier</a>
                <?php
                }
                ?>
            </div>

        </div>
        <?php
    }
 ?>
        <?php if (isset($nbArticles) && $nbArticles > 0) {?>

            <nav class="relative z-0 inline-flex rounded-md shadow-sm -space-x-px mt-5 mx-auto" aria-label="Pagination">
                <a href="./?page=<?= $currentPage - 1 ?>" class="<?= ($currentPage == 1) ? "disabled" : "" ?> relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50"><</a>

                <?php for($page = 1; $page <= $pages; $page++): ?>
                    <!-- Lien vers chacune des pages (activé si on se trouve sur la page correspondante) -->
                    <a href="./?page=<?= $page ?>" class="<?= ($currentPage == $page) ? "active disabled" : "" ?> z-10 text-indigo-600 relative inline-flex items-center px-4 py-2 border text-sm font-medium"><?= $page ?></a>
                <?php endfor ?>


                <!-- Current: "z-10 bg-indigo-50 border-indigo-500 text-indigo-600", Default: "bg-white border-gray-300 text-gray-500 hover:bg-gray-50" -->

                <a href="./?page=<?= $currentPage + 1 ?>" class="<?= ($currentPage == $pages) ? "disabled" : "" ?> relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">></a>
            </nav>

        <?php
        }
        if (isset($_GET['deleted'])){
            ?>
            <div class="bg-red-300 text-red-900 p-2 rounded-lg border-red-900 w-max mx-auto mt-4">Le post numero <?= $_GET['deleted'] ?> a été supprimer</div>

            <?php
        }

    }
    else{
    ?>
    <div class="col-md-12 custom-label">
        <h1>Bonjour, vous n'etes pas connecté.</h1>
    </div>
    <div>
        <a href='../../index.php' >Connexion</a>
    </div>
</div>

<?php
    }
    include('../includes/footer.php');
    ?>


<style>
    .disabled{
        pointer-events: none;
    }
    .active{
        background: rgb(160, 196, 238);
    }
</style>
