<?php
//inclusion du fichier de la connection
include('../config/connection.php');

include('../includes/header.php');
include('../utils/function.php');



if (isset($_SESSION['id']) && $_SERVER["REQUEST_METHOD"] == "POST") {

    $query = $pdo->prepare("SELECT * FROM users WHERE email = ? ");
    $query->execute(array($_SESSION['email']));
    $result = $query->fetch();
    $id = $result['id'];

    $title = test_input($_REQUEST['title']);
    $desc = test_input($_REQUEST['desc']);


    $query = "INSERT into `posts` (title, description, user_id)
              VALUES ('$title', '$desc', '$id')";

    $create = $pdo->prepare($query);
    $create->execute();

    header('Location: /');
}
?>
<div class="grid md:w-2/3 w-full">
    <form action="" method="post" >

        <div class="mb-6">
            <label for="title" class="custom-label">Titre</label>
            <input name="title" type="text" id="title" class="custom-input" required>
        </div>
        <div class="mb-6">
            <label for="desc" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Description</label>
            <textarea name="desc" id="desc" class="custom-input" required></textarea>
        </div>

        <div class="w-full">
            <button type="submit" class="custom-blue-button">valider</button>
        </div>



    </form>
    <a href="/" class="text-blue-500">Tous les posts</a>
</div>



<?php

include('../includes/footer.php'); ?>
