<?php
//inclusion du fichier de la connection
include('../config/connection.php');

include('../includes/header.php');
include('../utils/function.php');
$message = null;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = test_input($_POST["title"]);
    $desc = test_input($_POST["desc"]);
    $postId =$_POST['postId'];

    $sql = "UPDATE posts SET title = '$title', description = '$desc' WHERE id = '$postId'";

    // On prépare la requête
    $query = $pdo->prepare($sql);

    // On exécute
    $query->execute();

    $message = ' Mise a jour effectuer avec succès !';


}

if (isset($_SESSION['email']) ) {

    if (isset($_GET['postId'])){
        $postId = $_GET['postId'];
        $sql = "SELECT * FROM `posts` WHERE id = '$postId';";

        // On prépare la requête
        $query = $pdo->prepare($sql);

        // On exécute
        $query->execute();

        $post = $query->fetch();
        if ($post['user_id'] != $_SESSION['id']){
            $post = null;
            header('location: /');
        }else{
            if (isset($_GET['delete'])){
                $postId = $_GET['postId'];
                $sql = "DELETE FROM `posts` WHERE id = '$postId'";

                // On prépare la requête
                $query = $pdo->prepare($sql);

                // On exécute
                $query->execute();

                header("Location: /src/post/index.php?deleted=$postId");
            }
        }



    }

}

?>


<div class="grid md:w-2/3 w-full ">
    <?php if (isset($_SESSION['email']) and isset($post)) { ?>
    <form action="" method="post">
        <div class="mb-6">
            <label for="title" class="custom-label">Titre</label>
            <input name="title" type="text" id="title" class="custom-input" value="<?= $post['title'] ?>" required>
        </div>
        <div class="mb-6">
            <label for="desc" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Description</label>
            <textarea name="desc" id="desc" class="custom-input" required><?= $post['description'] ?></textarea>
        </div>
        <input type="hidden" name="postId" value="<?= $post['id'] ?>">

        <div class="w-full">
            <button type="submit" class="custom-blue-button">valider</button>
            <button onclick="resetInput()" type="button" class="custom-button bg-amber-400 hover:bg-amber-500 text-black ">Vider</button>
            <a href="edit.php?postId=<?= $post['id'] ?>&delete=true" class="float-right custom-button bg-red-600 hover:bg-red-700 text-white ">Supprimer</a>
        </div>
       
    </form>

    <?php if (isset($message) ) { ?>
        <div class="bg-green-300 text-green-900 p-2 rounded-lg border-green-900 w-max mx-auto mt-4"><?= $message ?></div>
    <?php } ?>
</div>

        <?php

    }
    else{
        header('location: /');
    }
include('../includes/footer.php');
?>

<script>
    function resetInput(){
        document.querySelector("#title").value = null;
        document.querySelector("#desc").value = null;
    }
</script>
