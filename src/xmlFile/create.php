<?php
//inclusion du fichier de la connection
include('../config/connection.php');
include('../utils/function.php');

include('../includes/header.php');



if (isset($_SESSION['email'])) {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        $xml = new DOMDocument("1.0");

        // It will format the output in xml format otherwise
        // the output will be in a single row
        $xml->formatOutput = true;
        $fitness = $xml->createElement("users");
        $xml->appendChild($fitness);

        $user = $xml->createElement("user");
        $fitness->appendChild($user);

        foreach ($_POST as $key => $attr) {
            ${$key} = $xml->createElement($key, $attr);
            $user->appendChild(${$key});
        }


        $filename = "user_" . $_POST['nom'] . "_" . $_POST['prenom'] . ".xml";
        $xml->save($filename);

        downloadFile($filename);

    }
    if (isset($_GET['allUser'])) {
        $sql = "Select * from users";
        // On prépare la requête
        $query = $pdo->prepare($sql);
        // On exécute
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        if ($result > 0) {
            $xml = new DOMDocument("1.0");

            // It will format the output in xml format otherwise
            // the output will be in a single row
            $xml->formatOutput = true;
            $fitness = $xml->createElement("users");
            $xml->appendChild($fitness);
            foreach ($result as $userBdd) {
                $user = $xml->createElement("user");
                $fitness->appendChild($user);


                foreach ($userBdd as $key => $attr) {
                    ${$key} = $xml->createElement($key, $attr);
                    $user->appendChild(${$key});

                }


                /*
                 *
                $description = $xml->createElement("description", $row['description']);
                $user->appendChild($description);

                $user=$xml->createElement("user");
                $fitness->appendChild($user);

                $uid=$xml->createElement("uid", $row['uid']);
                $user->appendChild($uid);

                $uname=$xml->createElement("uname", $row['uname']);
                $user->appendChild($uname);



                $role=$xml->createElement("role", $row['role']);
                $user->appendChild($role);

                $pic=$xml->createElement("pic", $row['pic']);
                $user->appendChild($pic);
                */

            }
            $filename = "all_users.xml";
            $xml->save($filename);
            downloadFile($filename);

        } else {
            echo "error";
        }

    }

    if (isset($_GET['personal'])) {
        $id = $_SESSION['id'];
        $sql = "Select * from users WHERE id = '$id'";
        // On prépare la requête
        $query = $pdo->prepare($sql);
        // On exécute
        $query->execute();
        $userBdd = $query->fetch(PDO::FETCH_ASSOC);

        if ($userBdd) {
            $xml = new DOMDocument("1.0");

            // It will format the output in xml format otherwise
            // the output will be in a single row
            $xml->formatOutput = true;
            $fitness = $xml->createElement("users");
            $xml->appendChild($fitness);

            $user = $xml->createElement("user");
            $fitness->appendChild($user);

            foreach ($userBdd as $key => $attr) {
                ${$key} = $xml->createElement($key, $attr);
                $user->appendChild(${$key});

            }

            $filename = "user_" . $userBdd['username'] . ".xml";
            $xml->save($filename);

            downloadFile($filename);



        }

    }
}


?>
<div class="grid md:w-2/3 w-full">
    <div>
        <h2 class="text-2xl mb-6 custom-label">XML base de données</h2>

        <div class="md:flex grid">
            <div class="m-2 w-full md:w-auto">
                <a class="custom-button bg-gray-800 dark:bg-gray-400 w-full" href="create.php?allUser=true">Tous les utilisateurs</a>
            </div>
            <div class="m-2 w-full md:w-auto">
                <a class="custom-button bg-gray-800 dark:bg-gray-400 w-full" href="create.php?personal=true">Mes données
                    personnelles</a>
            </div>

        </div>

    </div>


    <form action="" class="mt-10" method="post">
        <h2 class="text-2xl mb-6 custom-label">XML Personnalisé</h2>
        <div class="md:flex">
            <div class="m-2 md:w-1/2">
                <label for="email">Email</label>
                <input id="email" name="email" class="custom-input" type="email" required/>

            </div>
            <div class="m-2 md:w-1/2">
                <label for="username">Nom d'utilisateur</label>
                <input id="username" name="username" class="custom-input" type="text" required/>
            </div>

        </div>
        <div class="m-2 ">
            <label for="password">Mot de passe</label>
            <input id="password" name="password" class="custom-input" type="password" required/>

        </div>

        <div class="md:flex">
            <div class="m-2 md:w-1/2">
                <label for="nom">Nom</label>
                <input id="nom" name="nom" class="custom-input" type="text" required/>

            </div>
            <div class="m-2 md:w-1/2">
                <label for="prenom">Prénom</label>
                <input id="prenom" name="prenom" class="custom-input" type="text"/>
            </div>

        </div>

        <div class="md:flex">
            <div class="m-2 md:w-2/3">
                <label for="add">Adresse</label>
                <input id="add" name="add" class="custom-input" type="text"/>

            </div>
            <div class="m-2 md:w-1/3">
                <label for="date">Date de naissance</label>
                <input id="date" name="date" class="custom-input" type="date"/>
            </div>

        </div>

        <div class="md:flex">
            <div class="m-2 md:w-1/2">
                <label for="lat">Latitude</label>
                <input id="lat" name="lat" class="custom-input" type="text"/>

            </div>
            <div class="m-2 md:w-1/2">
                <label for="long">Longitude</label>
                <input id="long" name="long" class="custom-input" type="text"/>
                <div id="loading" class="hidden pl-3"></div>
            </div>


        </div>


        <button id="submitBt" name="submit" class="custom-blue-button desactiveButton" type="submit">Enregistrer
        </button>
    </form>


</div>


<?php

include('../includes/footer.php'); ?>


<script>
    var x = document.getElementById("demo");
    var lat = document.querySelector("#lat");
    var long = document.querySelector("#long");

    if (navigator.geolocation) {
        document.querySelector("#loading").classList.add("displayLoad");
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }

    function showPosition(position) {

        lat.value = position.coords.latitude;
        long.value = position.coords.longitude;
        document.querySelector("#loading").classList.remove("displayLoad");
        document.querySelector("#submitBt").classList.remove("desactiveButton");

    }

    lat.addEventListener("input", checkCoordValue);
    long.addEventListener("input", checkCoordValue);

    function checkCoordValue() {
        if (lat.value && long.value) {
            document.querySelector("#submitBt").classList.remove("desactiveButton");
        }
    }


</script>

<style>
    .displayLoad {
        display: inline-block;
    }

    .desactiveButton {
        pointer-events: none;
    }


    #loading {
        position: absolute;
        width: 50px;
        height: 50px;
        border: 3px solid rgba(255, 255, 255, .3);
        border-radius: 50%;
        border-top-color: #fff;
        animation: spin 1s ease-in-out infinite;
        -webkit-animation: spin 1s ease-in-out infinite;
    }

    @keyframes spin {
        to {
            -webkit-transform: rotate(360deg);
        }
    }

    @-webkit-keyframes spin {
        to {
            -webkit-transform: rotate(360deg);
        }
    }
</style>