<?php
// demarage du session

include('../models/User.php');

include('../includes/header.php');
include('../utils/function.php');


if (isset($_POST['submit'])) {
    $array = array(
        "email" => test_input($_POST['email']),
        "password" => test_input($_POST['password']),
        "username" => test_input($_POST['username']),
    );


    $user = new User($array);

    $result = $user->save();

    $message = $result[1];

    if ($result[0]) {
        try {

            include('../config/connection.php');

            $verif = $pdo->prepare("SELECT * FROM users WHERE email = ? ");
            $verif->execute(array($user->getEmail()));
            $user = $verif->fetch();

            $_SESSION['email'] = $user['email'];
            $_SESSION['id'] = $user['id'];

            header('Location: /');
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
?>

<div class="md:w-1/2 w-full">
    <form action="" class="register" method="post">
        <h2 class="text-2xl mb-6 custom-label">Créer un compte</h2>
        <input name="email" class="custom-input" type="email" placeholder="email" />

        <input name="username" class="custom-input" type="text" placeholder="username" />

        <input name="password" class="custom-input" type="password" placeholder="password" />

        <div class="show_info text-sm mb-4 w-max text-red-400 hidden">username already taken</div>

        <button name="submit" class="custom-blue-button" type="submit" >Enregistrer</button>
    </form>

    <?php if (isset($message)){
    ?>
        <div class="mt-4 p-3 bg-red-300 border border-red-800 text-red-900 rounded-xl text-center"><?= $message ?> </div>
        <?php
    }
    ?>
</div>


<?php

include('../includes/footer.php'); ?>