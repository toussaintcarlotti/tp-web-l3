<?php
// demarage du session
include('../config/connection.php');
include('../models/User.php');

include('../includes/header.php');
include('../utils/function.php');


if (isset($_SESSION['id']) && isset($_GET['userId'] )) {
    $id = $_SESSION['id'];

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        try {
            $array = array(
                    "id" => $_SESSION['id'],
                    "email" => test_input($_POST['email']),
                    "username" => test_input($_POST['username']),
                    "nom" => test_input($_POST['nom']),
                    "prenom" => test_input($_POST['prenom']),
                    "add" => test_input($_POST['add']),
                    "dateNaiss" => test_input($_POST['date']),
            );

            $user = new User($array);
            $result = $user->update();

            $message = $result[1];
        }catch (PDOException $e){
            if ($e->getCode() == 23000)
                $errorMess = "L'email ou le nom d'utilisateur est déjà utilisé !";
            else
                $errorMess = $e->getCode();
        }



    }
    $query = $pdo->prepare("SELECT * FROM users WHERE id = '$id' ");
    $query->execute();

    $user = $query->fetch();

}




?>

    <div class="md:w-2/3 w-full">
        <form action="" class="register" method="post">
            <h2 class="text-2xl mb-6 custom-label">Mon profil</h2>
            <div class="md:flex">
                <div class="m-2 md:w-1/2">
                    <label for="email">Email</label>
                    <input id="email" name="email" class="custom-input" type="email" value="<?= $user['email'] ?>" />

                </div>
                <div class="m-2 md:w-1/2">
                    <label for="username">Nom d'utilisateur</label>
                    <input id="username" name="username" class="custom-input" type="text" value="<?= $user['username'] ?>"/>
                </div>

            </div>

            <div class="md:flex">
                <div class="m-2 md:w-1/2">
                    <label for="nom">Nom</label>
                    <input id="nom" name="nom" class="custom-input" type="text"  value="<?= $user['nom'] ?>" />

                </div>
                <div class="m-2 md:w-1/2">
                    <label for="prenom">Prénom</label>
                    <input id="prenom" name="prenom" class="custom-input" type="text"  value="<?= $user['prenom'] ?>"/>
                </div>

            </div>

            <div class="md:flex">
                <div class="m-2 md:w-2/3">
                    <label for="add">Adresse</label>
                    <input id="add" name="add" class="custom-input" type="text"  value="<?= $user['adresse'] ?>" />

                </div>
                <div class="m-2 md:w-1/3">
                    <label for="date">Date de naissance</label>
                    <input id="date" name="date" class="custom-input" type="date"  value="<?= $user['dateNaiss'] ?>"/>
                </div>

            </div>


            <button name="submit" class="custom-blue-button" type="submit" >Enregistrer</button>
        </form>

        <?php if (isset($errorMess)){
            ?>
            <div class="mt-4 p-3 bg-red-300 border border-red-800 text-red-900 rounded-xl text-center"><?= $errorMess ?> </div>
            <?php
        }
        ?>

        <?php if (isset($message) ) { ?>
            <div class="bg-green-300 text-green-900 p-2 rounded-lg border-green-900 w-max mx-auto mt-4"><?= $message ?></div>
        <?php } ?>
    </div>


<?php

include('../includes/footer.php'); ?>


