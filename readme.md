<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/github_username/repo_name">
    <h3 align="center">Tp web L3</h3>
    </a>

</div>

<!-- Css -->
## Css

Pour le css j'ai utilisé le framwork <a href="https://tailwindcss.com/"> tailwind css </a>


<!-- Installation -->
## Installation


Lancer la commande afin d'installer les dépendence nécessaire (c'est juste pour tailwind)
* npm
  ```sh
  npm install
  ```
  ou
  ```sh
  yarn install
  ```

### Base de données

J'ai déposer un fichier d'exportation (dump) sql à la racine du projet (tp_web_carlotti_bdd.sql)

La configuration de connection a la base de donnée s'effectue dans le fichier src/config/connection.php

